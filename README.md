# Metervreter

Het doel van dit project is om de meterstand van een slimme meter door te geven 
aan het netwerkbeheer programma prtg.

## onderdelen

Omdat de slimme meter eens in de 10 seconden data streamt en prtg de stand eens 
in de zoveel tijd wil controleren bestaat dit project uit twee delen:

*  server
*  probe

### Server
De taak van de server is om de data van de slimme meter op te vangen en de stand
intern bij te werken, hiernaast accepteerd de server ook Tcp verbindingen
waarmee de meterstand kan worden opgevraagd.

### Probe
De Probe is een script dat de meterstand opvraagt van de server en deze
informatie in een json formaat uitprint zodat het kan woorden gebruikt door
het netwerkbeheer programma [prtg](https://www.paessler.com/prtg).
