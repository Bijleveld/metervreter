#!/usr/bin/env python3
import socket

from time import sleep
from paepy.ChannelDefinition import CustomSensorResult

SERVER_NAME = '192.168.1.13'
SERVER_PORT = 1234
ENCODING = "utf-8"

def bot():
    global SERVER_NAME, SERVER_PORT
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((SERVER_NAME, SERVER_PORT))
        sleep(2)
        s.send(bytes("meter1\n", ENCODING))
        meter1 = float(str(s.recv(1024), ENCODING).replace(",", "."))
        sleep(.1)
        s.send(bytes("meter2\n", ENCODING))
        meter2 = float(str(s.recv(1024), ENCODING).replace(",", "."))

    sensor = CustomSensorResult()
    sensor.add_channel("meter1 verbruik", mode="Difference", value=int(meter1 * 1000), unit="Wh", primary_channel=True)
    sensor.add_channel("meter2 verbruik", mode="Difference", value=int(meter2 * 1000), unit="Wh")
    sensor.add_channel("meter1 stand", value=meter1, unit="kWh", is_float=True)
    sensor.add_channel("meter2 stand", value=meter2, unit="kWh", is_float=True)
    print(sensor.get_json_result())


if __name__ == '__main__':
    bot()
