[Console]::TreatControlCAsInput = $True
$Global:Listener = [HashTable]::Synchronized(@{})
$Global:hash = [hashtable]::Synchronized(@{})
[double]$hash.Meterstand1 = -1
[double]$hash.Meterstand2 = -1
$hash.stop = $False
$hash.port = "1234"
$hash.ip = "0.0.0.0"
$Global:Connections = [System.Collections.ArrayList]::Synchronized(@{})
$Global:CnQueue = [System.Collections.Queue]::Synchronized((New-Object System.collections.queue))
$Global:space = [RunSpaceFactory]::CreateRunspace()

$space.Open()
$space.SessionStateProxy.setVariable("CnQueue", $CnQueue)
$space.SessionStateProxy.setVariable("Listener", $Listener)
$space.SessionStateProxy.setVariable("Hash", $hash)
$Global:newPowerShell = [PowerShell]::Create()
$newPowerShell.Runspace = $space

$Timer = New-Object Timers.Timer
$Timer.Enabled = $true
$Timer.Interval = 1000
Register-ObjectEvent -SourceIdentifier MonitorClientConnection -InputObject $Timer -EventName Elapsed -Action {
    While($CnQueue.count -ne 0) {
        $client = $CnQueue.Dequeue()
        $Global:Connections.Add($client)
        $newRunspace = [RunSpaceFactory]::CreateRunspace()
        $newRunspace.Open()
        $newRunspace.SessionStateProxy.setVariable("client", $client)
        $newRunspace.SessionStateProxy.setVariable("Hash", $hash)
        $newPowerShell = [PowerShell]::Create()
        $newPowerShell.Runspace = $newRunspace

        $process = {
            $stream = $client.GetStream();
            [console]::WriteLine("{0} >> Client process spawned" -f(Get-Date).ToString())
            $buffer = new-object System.Byte[] 8192
            $encoding = new-object System.Text.AsciiEncoding
            $writer = New-Object System.IO.StreamWriter($stream)
            $writer.AutoFlush = $true
            while (-not ($client.Client.Poll(1,[System.Net.Sockets.SelectMode]::SelectRead) -and $client.Client.Available -eq 0)){
                while ($stream.DataAvailable){
                    $rawresponse = $stream.Read($buffer, 0, 8192)
                    $response = $encoding.GetString($buffer, 0, $rawresponse).trim()
                    [console]::WriteLine("{0} >> msg: $response" -f(Get-Date).ToString())
                    if ($response -like "meter1"){
                        $writer.WriteLine("{0}" -f $hash.Meterstand1)
                    } elseif ($response -like "meter2"){
                        $writer.WriteLine("{0}" -f $hash.Meterstand2)
                    }
                }
                Start-Sleep -Milliseconds 200
            }
            $Global:Connections.Remove($client)
            [console]::WriteLine("{0} >> Client Disconnected" -f(Get-Date).ToString())
            $writer.Close()
            $stream.Close()
            $client.Close()
        }
        $jobHandle = $newPowerShell.AddScript($process).BeginInvoke()
        #jobHandle you need to save for future to cleanup
    }
}

$listener = {
    $endpoint = new-object System.Net.IPEndPoint ([system.net.ipaddress]::any, $hash.port)
    $Global:Listener['listener'] = New-Object System.Net.Sockets.TcpListener($endpoint)
    $Global:Listener['listener'].Start()
    [console]::WriteLine("Listening on $($hash.port)")
    while ($True) {
        Start-Sleep -Milliseconds 200
        $c = $Global:Listener['listener'].AcceptTcpClient()
        If($c -ne $Null -And ($hash.Meterstand1 -lt 0 -Or $hash.Meterstand2 -lt 0)){
            [console]::WriteLine("{0} >> Client connection refused, no measurement yet." -f(Get-Date).ToString())
            $c.Close()
            Continue
        }
        elseIf($c -ne $Null -and $Global:Listener['listener']) {
            [console]::WriteLine("{0} >> Accepted Client " -f(Get-Date).ToString())
            $CnQueue.Enqueue($c)
        }
        Else {
            [console]::WriteLine("Shutting down")
            $Global:Listener['listener'].Stop()
            Break
        }
    }
}

$portListener = {
    $port = [System.IO.Ports.SerialPort]::getportnames()

    if ($port.Count -eq 0) {
        [console]::WriteLine("No port found, exiting....")
    }

    # create object to communicate with port
    $serialPort = New-Object -Typename System.IO.Ports.SerialPort -ArgumentList $port, 115000
    #open connection
    $serialPort.Open()

    # read while the port is open
    $first = 1
    do {
        $line = $serialPort.ReadLine()
        if($hash.stop){
            $serialPort.Close()
        }
        
        If (-Not $first) {
            if ($line -like "*1-0:1.8.1*"){
                [double]$hash.Meterstand1 = $line.split("(")[1].split("*")[0]
            } elseif ($line -like "*1-0:1.8.2*") {
                [double]$hash.Meterstand2 = $line.split("(")[1].split("*")[0]
            }
        }

        If ($first -and $line[0] -eq "!") {
            $first = 0
        }
    } While ($serialPort.isOpen)
}

$Global:portSpace = [RunSpaceFactory]::CreateRunspace()
$portSpace.Open()
$portSpace.SessionStateProxy.setVariable("Hash", $hash)
$Global:portShell = [PowerShell]::Create()
$portShell.Runspace = $portSpace
$Global:portHandle = $portShell.AddScript($portListener).BeginInvoke()

$Timer.Start()
$Global:handle = $newPowerShell.AddScript($listener).BeginInvoke()

while ($true){
    If ($Host.UI.RawUI.KeyAvailable -and ($Key = $Host.UI.RawUI.ReadKey("AllowCtrlC,NoEcho,IncludeKeyUp"))) {
        If ([Int]$Key.Character -eq 3) {
            Write-Host ""
            Write-Warning "CTRL-C was used - Shutting down any running jobs before exiting the script."
            Unregister-Event -SourceIdentifier MonitorClientConnection
            $Timer.Stop()
            $Timer.Dispose()
            $Global:hash.stop = $True
            foreach ($conn in $Global:Connections) {
                $conn.Close()
            }
            if ($Global:Listener['listener']) {
                $Global:Listener['listener'].Stop()
                Write-Host "Shutting down listener.."
            }
            $newPowerShell.Stop()
            [Console]::TreatControlCAsInput = $False
            Exit 0
        }
        # Flush the key buffer again for the next loop.
        $Host.UI.RawUI.FlushInputBuffer()
    }
}
